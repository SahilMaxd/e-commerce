import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { Navbar, Nav, FormControl, Button, Form, InputGroup, Container, Row, Col } from 'react-bootstrap';
import Home from './home'
import { Link, Route} from 'react-router-dom'

class App extends Component{
  render() {
    return (
        <div>
          <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="/">Ecom</Navbar.Brand>
            <Form inline className='mb-1'>
              <InputGroup className=''>
                <FormControl placeholder="Search" />
                <InputGroup.Append>
                  <Button>Search</Button>
                </InputGroup.Append>
              </InputGroup>
            </Form>
          </Navbar>
          <Route exact={true} path='/' component={Home} />
        </div>
    );
  };
}

export default App;
